#include "args.h"
#include "bmp_read.h"
#include "bmp_write.h"
#include "file.h"
#include "rotate.h"
#include "utils.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  args_t *parsed_args = parse_arguments(argc, argv);
  enum validation_status v_status = validate_args(parsed_args);
  if (v_status) {
    ERROR_MSG("Validation failed!");
    ERROR_MSG(validation_status_messages[v_status]);
    printf("Usage: %s <image_file> <transformed-image> <angle>\n", argv[0]);
    printf("<image_file>:        BMP image to be transformed\n");
    printf("<transformed-image>: Output BMP image name\n");
    printf("<angle>:             Angel of rotation. Allowed values: -270, "
           "-180, -90, 0, 90, 180, "
           "270\n");
    return EXIT_FAILURE;
  }

  printf("Processing file: %s\n", parsed_args->filename);

  // Open the input file
  FILE *in = fopen(parsed_args->filename, READ_BINARY);
  if (!in) {
    ERROR_MSG("Could not open input file!");
    return EXIT_FAILURE;
  }

  bmp_image_t image;
  image.free = free_bmp;

  // Read the image
  enum read_status r_status = from_bmp(in, &image);
  if (r_status) {
    ERROR_MSG("Reading failed!");
    ERROR_MSG(read_status_messages[r_status]);
    return EXIT_FAILURE;
  }

  printf("File read successfully!\n");
  printf("Image would be rotated by angle: %d degrees\n", parsed_args->angle);

  // rotate
  rotate(&image, parsed_args->angle);

  // Open the output file
  FILE *out = fopen(parsed_args->output_filename, WRITE_BINARY);
  if (!out) {
    ERROR_MSG("Could not open output file!");
    return EXIT_FAILURE;
  }

  // Write the image
  enum write_status w_status = to_bmp(out, &image);
  if (w_status) {
    ERROR_MSG("Writing failed!");
    ERROR_MSG(write_status_messages[w_status]);
    return EXIT_FAILURE;
  }

  image.free(&image);
  free(parsed_args);

  return EXIT_SUCCESS;
}
