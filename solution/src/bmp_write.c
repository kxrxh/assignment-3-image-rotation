#include "bmp_write.h"
#include "bmp.h"
#include "utils.h"
#include <stdint.h>

const char *write_status_messages[] = {
    [WRITE_OK] = "Write operation successful",
    [WRITE_ERROR] = "Write operation failed",
    [WRITE_INVALID_FUNCTION_ARGUMENT] =
        "Invalid function argument. May be some of arguments are NULL",
};

static struct bmp_header get_default_header(bmp_image_t const *img,
                                            uint32_t padding) {
  uint32_t pixel_data_size =
      (uint32_t)((img->width + padding) * img->height * sizeof(pixel_t));
  return (struct bmp_header){.bfType = IMAGE_TYPE_BMP,
                             .bfileSize =
                                 pixel_data_size + sizeof(struct bmp_header),
                             .bfReserved = 0,
                             .bOffBits = sizeof(struct bmp_header),
                             .biSize = BITMAP_HEADER_SIZE,
                             .biWidth = img->width,
                             .biHeight = img->height,
                             .biPlanes = 1,
                             .biBitCount = SUPPORTED_BIT_COUNT,
                             .biCompression = 0,
                             .biSizeImage = 0,
                             .biXPelsPerMeter = 0,
                             .biYPelsPerMeter = 0,
                             .biClrUsed = 0,
                             .biClrImportant = 0};
}

enum write_status to_bmp(FILE *out, bmp_image_t const *img) {
  if (!out) {
    ERROR_MSG("Invalid output file!");
    return WRITE_INVALID_FUNCTION_ARGUMENT;
  }

  uint32_t padding = calculate_padding(img->width);

  struct bmp_header header = get_default_header(img, padding);

  // Write the header
  if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
    return WRITE_ERROR;
  }

  // Write pixel data with padding
  for (size_t i = 0; i < img->height; ++i) {
    pixel_t *row = img->pixels + i * img->width;
    size_t write_pixels = fwrite(row, sizeof(struct pixel), img->width, out);

    // Write padding
    uint8_t padding_data[4] = {0}; // Assuming padding is at most 3 bytes
    size_t write_padding = fwrite(padding_data, sizeof(uint8_t), padding, out);

    if (write_pixels != img->width || write_padding != padding) {
      return WRITE_ERROR;
    }
  }

  return WRITE_OK;
}
