#include "args.h"
#include "utils.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define ARG_COUNT 4

const char *validation_status_messages[] = {
    [VALIDATION_SUCCESS] = "Validation successful",
    [VALIDATION_NULL_POINTER] = "Validation failed: NULL pointer",
    [VALIDATION_BAD_ANGLE] = "Validation failed: Bad angle",
    [VALIDATION_NULL_FILENAME] = "Validation failed: NULL filename",
};

args_t *parse_arguments(int argc, char **argv) {
  if (argc != ARG_COUNT) {
    return NULL;
  }
  args_t *args = malloc(sizeof(args_t));
  if (args == NULL) {
    fprintf(stderr, "Failed to allocate memory for Args struct\n");
    exit(EXIT_FAILURE);
  }
  args->filename = argv[1];
  args->output_filename = argv[2];
  args->angle = (int16_t)atoi(argv[3]);

  return args;
}

enum validation_status validate_args(args_t *args) {
  if (!args)
    return VALIDATION_NULL_POINTER;

  if (args->angle % 90 || args->angle < -270 || args->angle > 270)
    return VALIDATION_BAD_ANGLE;

  if (!args->filename || !args->output_filename)
    return VALIDATION_NULL_FILENAME;

  return VALIDATION_SUCCESS;
}
