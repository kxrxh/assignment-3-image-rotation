#include "bmp.h"
#include <stdlib.h>

void free_bmp(bmp_image_t *image) {
  if (!image)
    return;
  if (image->pixels)
    free(image->pixels);
  image = NULL;
}

inline uint64_t calculate_padding(uint64_t width) {
  return (4 - (sizeof(pixel_t) * width) % 4) % 4;
}
