#include "rotate.h"
#include "bmp.h"
#include "utils.h"
#include <stdlib.h>

void rotate(bmp_image_t *image, int angle) {
  int loops = (angle / 90 + 4) % 4;
  printf("Rotating %d times\n", loops);
  for (int i = 0; i < loops; ++i, rotate_by_90(image))
    ;
}

static void transpose(bmp_image_t *image) {
  if (!image) {
    ERROR_MSG("Invalid image pointer!");
    exit(EXIT_FAILURE);
  }

  if (!image->pixels) {
    ERROR_MSG("No pixel data!");
    exit(EXIT_FAILURE);
  }

  if (!image->width || !image->height) {
    ERROR_MSG("Invalid image dimensions!");
    exit(EXIT_FAILURE);
  }
  
  pixel_t *transposed_image =
      malloc(image->width * image->height * sizeof(pixel_t));
  if (!transposed_image) {
    ERROR_MSG("Memory allocation failed!");
    exit(EXIT_FAILURE);
  }

  for (uint64_t i = 0; i < image->height; i++) {
    for (uint64_t j = 0; j < image->width; j++) {
      transposed_image[j * image->height + i] =
          image->pixels[i * image->width + j];
    }
  }

  // Free the pixel data
  free(image->pixels);

  // Swap width and height
  image->width = image->height ^ image->width;
  image->height = image->width ^ image->height;
  image->width = image->width ^ image->height;

  image->pixels = transposed_image;
}

void rotate_by_90(bmp_image_t *image) {
  /* We would use math-like rotation. Reverse and Transpose the order of
   * elements in each row */

  // Reverse the order of elements in each row
  for (uint64_t i = 0; i < image->height; i++) {
    for (uint64_t j = 0; j < image->width / 2; j++) {
      struct pixel tmp = image->pixels[i * image->width + j];
      image->pixels[i * image->width + j] =
          image->pixels[i * image->width + (image->width - j - 1)];
      image->pixels[i * image->width + (image->width - j - 1)] = tmp;
    }
  }

  transpose(image);
}
