#include "bmp_read.h"

#include "utils.h"
#include <stdlib.h>

const char *read_status_messages[] = {
    [READ_OK] = "Read operation successful",
    [READ_INVALID_SIGNATURE] = "Invalid BMP signature",
    [READ_INVALID_BITS] = "Invalid bits in the BMP file",
    [READ_INVALID_HEADER] = "Invalid BMP header",
    [READ_INVALID_FUNCTION_ARGUMENT] = "Invalid function argument",
    [READ_INVALID_BITS_PER_PIXEL] = "Invalid bits per pixel",
    [READ_SEEK_ERROR] = "Unable to seek in the file. File may be corrupted",
    [READ_INVALID_PIXEL_DATA] = "Invalid pixel data in the BMP file",
};

static int validate_header(const struct bmp_header *header) {

  // Size of the header should be 40
  if (header->biSize != BITMAP_HEADER_SIZE) {
    return 0;
  }

  // bfType should be "BM"
  if (header->bfType != IMAGE_TYPE_BMP) {
    return 0;
  }

  // Only 24-bit images are supported
  if (header->biBitCount != SUPPORTED_BIT_COUNT) {
    return 0;
  }

  // biWidth and biHeight should be > 0
  if (header->biWidth <= 0 || header->biHeight <= 0) {
    return 0;
  }
  return 1;
}

enum read_status from_bmp(FILE *in, bmp_image_t *img) {
  if (!in) {
    return READ_INVALID_FUNCTION_ARGUMENT;
  }

  struct bmp_header header;

  // Read the header
  if (sizeof(struct bmp_header) !=
      fread(&header, 1, sizeof(struct bmp_header), in)) {
    return READ_INVALID_HEADER;
  }

  // Validate the header data.
  int valid = validate_header(&header);
  if (!valid) {
    return READ_INVALID_HEADER;
  }

  // allocate memory for the pixel data
  pixel_t *pixels = calloc(sizeof(pixel_t), header.biWidth * header.biHeight);
  if (!pixels) {
    return READ_INVALID_FUNCTION_ARGUMENT;
  }

  // Move the file cursor to the pixel data location
  if (fseek(in, header.bOffBits, SEEK_SET)) {
    ERROR_MSG("Error seeking to pixel data");
    free(pixels);
    return READ_SEEK_ERROR;
  }

  uint64_t padding = calculate_padding(header.biWidth);
  for (size_t row_i = 0; row_i < header.biHeight; ++row_i) {

    pixel_t *row = pixels + row_i * header.biWidth;
    size_t fread_res = fread(row, sizeof(pixel_t), header.biWidth, in);

    if (fread_res != header.biWidth) {
      ERROR_MSG("Error reading pixel data");
      free(pixels);
      return READ_INVALID_PIXEL_DATA;
    }
    int shift_res = fseek(in, (long)padding, SEEK_CUR);
    if (shift_res) {
      ERROR_MSG("Error seeking to the next row");
      free(pixels);
      return READ_SEEK_ERROR;
    }
  }

  img->pixels = pixels;
  img->width = header.biWidth;
  img->height = header.biHeight;
  return READ_OK;
}
