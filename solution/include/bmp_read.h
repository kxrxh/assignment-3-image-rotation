#ifndef BMP_READ_H
#define BMP_READ_H

#include "bmp.h"

enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_FUNCTION_ARGUMENT,
  READ_INVALID_BITS_PER_PIXEL,
  READ_SEEK_ERROR,
  READ_INVALID_PIXEL_DATA,
};

// Array of human-readable messages corresponding to enum read_status.
extern const char *read_status_messages[];

// Reads a 24-bit BMP image and returns an image struct.
// If read fails, returns error code.
enum read_status from_bmp(FILE *in, bmp_image_t *img);

#endif
