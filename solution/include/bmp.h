#ifndef BMP_H
#define BMP_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#define IMAGE_TYPE_BMP 0x4D42 // Magic number for a BMP image "MB"
#define HEADER_SIZE 14        // Size of the BMP header
#define BITMAP_HEADER_SIZE 40 // Size of the BITMAPINFOHEADER
#define SUPPORTED_BIT_COUNT 24

// BMP file header structure
struct __attribute__((packed)) bmp_header {
  // Type of the BMP file, should be "BM" (0x4D42)
  uint16_t bfType;
  // Size of the BMP file in bytes
  uint32_t bfileSize;
  // Reserved for future use, typically set to 0
  uint32_t bfReserved;
  // Offset of the pixel data
  uint32_t bOffBits;

  // Size of the BITMAPINFOHEADER structure, should be 40
  uint32_t biSize;
  // Width of the image in pixels
  uint32_t biWidth;
  // Height of the image in pixels, positive for bottom-up,
  uint32_t biHeight;

  // Number of color planes, should be 1
  uint16_t biPlanes;
  // Number of bits per pixel (1, 4, 8, 16, 24, or 32)
  uint16_t biBitCount;

  // Compression type (0 for uncompressed)
  uint32_t biCompression;
  // Size of the image data in bytes (can be 0 for uncompressed images)
  uint32_t biSizeImage;
  // Horizontal resolution in pixels per meter
  uint32_t biXPelsPerMeter;
  // Vertical resolution in pixels per meter
  uint32_t biYPelsPerMeter;
  // Number of colors in the color palette, or 0 to default to 2^n
  uint32_t biClrUsed;
  // Number of important colors, or 0 when every color is important
  uint32_t biClrImportant;
};

// Pixel structure for 24-bit images
struct __attribute__((packed)) pixel {
  uint8_t b;
  uint8_t g;
  uint8_t r;
};

typedef struct pixel pixel_t;

// Represents a BMP image with support for both 24-bit.
struct bmp_image {
  uint64_t width, height;
  pixel_t *pixels; // Pixel data for 24-bit images
  void (*free)(struct bmp_image
                   *); // Function to free allocated memory by the pixel data.
};

typedef struct bmp_image bmp_image_t;

// Standard function to free allocated memory of a BMP image pixels.
void free_bmp(bmp_image_t *image);

// Calculates padding of pixels for 24-bit images
uint64_t calculate_padding(uint64_t width);
#endif
