#ifndef ROTATE_H
#define ROTATE_H

#include "bmp.h"

// Rotates the image by the given angle
void rotate(bmp_image_t *image, int angle);

// Rotates the image by 90 degrees
void rotate_by_90(bmp_image_t *image);

#endif
