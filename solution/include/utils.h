#ifndef UTILS_H
#define UTILS_H

#define ERROR_MSG(msg)                                                         \
  do {                                                                         \
    fprintf(stderr, "Error: %s\n", msg);                                       \
  } while (0)

#endif
