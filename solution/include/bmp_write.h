#ifndef BMP_WRITE_H
#define BMP_WRITE_H

#include "bmp.h"

enum write_status {
  WRITE_OK = 0,
  WRITE_INVALID_FUNCTION_ARGUMENT,
  WRITE_ERROR,
};

extern const char *write_status_messages[];

// Writes a 24-bit BMP image from an image struct.
enum write_status to_bmp(FILE *out, bmp_image_t const *img);

#endif
