#ifndef ARGS_H
#define ARGS_H

#include <stdint.h>

/* Define a struct to store the command line arguments */
struct arguments {
  const char *filename;        // The name of the file to process
  const char *output_filename; // The name of the output file
  int16_t angle;               // The angle value
};

typedef struct arguments args_t;

enum validation_status {
  VALIDATION_SUCCESS = 0,
  VALIDATION_NULL_POINTER,
  VALIDATION_BAD_ANGLE,
  VALIDATION_NULL_FILENAME,
};

extern const char *validation_status_messages[];

/* Function to parse the command line arguments and return an Args struct with
 * the file name and angle value */
args_t *parse_arguments(int argc, char **argv);

/* Function to validate the parsed arguments. */
enum validation_status validate_args(args_t *args);

#endif
